//
//  HveSchema.h
//  HikVisionExt
//
//  Created by Dan on 16.09.2021.
//

#import "HveMain.h"

#pragma mark - ResponseStatus

@interface HveResponseStatus : NseObject

@property NSString *requestUrl;
@property NSInteger statusCode;
@property NSString *statusString;
@property NSString *subStatusCode;

+ (instancetype)from:(HVEResponseStatus *)sdkResponseStatus;

@end

#pragma mark - ActivateStatus

@interface HveActivateStatus : NseObject

@property BOOL activated;

+ (instancetype)from:(HVEActivateStatus *)sdkActivateStatus;

@end

#pragma mark - PublicKey

@interface HvePublicKey : NseObject

@property NSString *key;

+ (instancetype)from:(HVEPublicKey *)sdkPublicKey;
- (HVEPublicKey *)to;

@end

#pragma mark - Challenge

@interface HveChallenge : NseObject

@property NSString *key;

+ (instancetype)from:(HVEChallenge *)sdkChallenge;

@end

#pragma mark - ActivateInfo

@interface HveActivateInfo : NseObject

@property NSString *password;

+ (instancetype)from:(HVEActivateInfo *)sdkActivateInfo;
- (HVEActivateInfo *)to;

@end

#pragma mark - DeviceInfo

@interface HveDeviceInfo : NseObject

@property NSString *deviceName;
@property NSString *deviceId;
@property NSString *deviceDescription;
@property NSString *deviceLocation;
@property NSString *systemContact;
@property NSString *model;
@property NSString *serialNumber;
@property NSString *macAddress;
@property NSString *firmwareVersion;
@property NSString *firmwareReleasedDate;
@property NSString *bootVersion;
@property NSString *bootReleasedDate;
@property NSString *hardwareVersion;
@property NSString *encoderVersion;
@property NSString *encoderReleasedDate;
@property NSString *decoderVersion;
@property NSString *decoderReleasedDate;
@property NSString *deviceType;
@property NSInteger telecontrolId;
@property BOOL supportBeep;
@property NSString *firmwareVersionInfo;

+ (instancetype)from:(HVEDeviceInfo *)sdkDeviceInfo;

@end

#pragma mark - Time

@interface HveTime : NseObject

@property NSString *timeMode;
@property NSString *localTime;
@property NSString *timeZone;

+ (instancetype)from:(HVETime *)sdkTime;
- (HVETime *)to;

@end

#pragma mark - NetworkInterface

@interface HveNetworkInterface : NseObject

@property NSString *id;
@property BOOL defaultConnection;
@property NSString *macAddress;

+ (instancetype)from:(HVENetworkInterface *)sdkNetworkInterface;

@end

@interface NSArray (HveNetworkInterface)

+ (instancetype)hveNetworkInterfaceFrom:(GList *)sdkNetworkInterfaces;

@end

#pragma mark - WPA

@interface HveWPA : NseObject

@property NSString *algorithmType;
@property NSString *sharedKey;
@property NSInteger wpaKeyLength;

+ (instancetype)from:(HVEWPA *)sdkWPA;
- (HVEWPA *)to;

@end

#pragma mark - WirelessSecurity

@interface HveWirelessSecurity : NseObject

@property NSString *securityMode;
@property HveWPA *wpa;

+ (instancetype)from:(HVEWirelessSecurity *)sdkWirelessSecurity;
- (HVEWirelessSecurity *)to;

@end

#pragma mark - Wireless

@interface HveWireless : NseObject

@property BOOL enabled;
@property NSString *ssid;
@property HveWirelessSecurity *wirelessSecurity;

+ (instancetype)from:(HVEWireless *)sdkWireless;
- (HVEWireless *)to;

@end

#pragma mark - IPv6Mode

@interface HveIPv6Mode : NseObject

@property NSString *ipv6AddressingType;

+ (instancetype)from:(HVEIPv6Mode *)sdkIPv6Mode;
- (HVEIPv6Mode *)to;

@end

#pragma mark - IPAddress

@interface HveIPAddress : NseObject

@property NSString *ipVersion;
@property NSString *addressingType;
@property HveIPv6Mode *ipv6Mode;

+ (instancetype)from:(HVEIPAddress *)sdkIPAddress;
- (HVEIPAddress *)to;

@end

#pragma mark - Video

@interface HveVideo : NseObject

@property NSString *videoCodecType;
@property NSInteger videoResolutionWidth;
@property NSInteger videoResolutionHeight;
@property NSString *videoQualityControlType;
@property NSInteger maxFrameRate;

+ (instancetype)from:(HVEVideo *)sdkVideo;
- (HVEVideo *)to;

@end

#pragma mark - Audio

@interface HveAudio : NseObject

@property BOOL enabled;
@property NSString *audioCompressionType;

+ (instancetype)from:(HVEAudio *)sdkAudio;
- (HVEAudio *)to;

@end

#pragma mark - StreamingChannel

@interface HveStreamingChannel : NseObject

@property HveVideo *video;
@property HveAudio *audio;

+ (instancetype)from:(HVEStreamingChannel *)sdkStreamingChannel;
- (HVEStreamingChannel *)to;

@end

#pragma mark - Grid

@interface HveGrid : NseObject

@property NSInteger rowGranularity;
@property NSInteger columnGranularity;

+ (instancetype)from:(HVEGrid *)sdkGrid;
- (HVEGrid *)to;

@end

#pragma mark - Layout

@interface HveLayout : NseObject

@property NSString *gridMap;

+ (instancetype)from:(HVELayout *)sdkLayout;
- (HVELayout *)to;

@end

#pragma mark - MotionDetectionLayout

@interface HveMotionDetectionLayout : NseObject

@property NSInteger sensitivityLevel;
@property HveLayout *layout;

+ (instancetype)from:(HVEMotionDetectionLayout *)sdkMotionDetectionLayout;
- (HVEMotionDetectionLayout *)to;

@end

#pragma mark - MotionDetection

@interface HveMotionDetection : NseObject

@property BOOL enabled;
@property HveGrid *grid;
@property HveMotionDetectionLayout *motionDetectionLayout;

+ (instancetype)from:(HVEMotionDetection *)sdkMotionDetection;
- (HVEMotionDetection *)to;

@end

#pragma mark - Endpoint

@interface HveEndpoint : NseObject

@property NSString *name;
@property NSString *host;
@property NSString *ip;
@property NSInteger port;
@property NSString *path;

+ (instancetype)from:(HVEEndpoint *)sdkEndpoint;
- (HVEEndpoint *)to;

@end
