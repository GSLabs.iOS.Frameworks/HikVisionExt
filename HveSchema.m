//
//  HveSchema.m
//  HikVisionExt
//
//  Created by Dan on 16.09.2021.
//

#import "HveSchema.h"

#pragma mark - ResponseStatus

@implementation HveResponseStatus

+ (instancetype)from:(HVEResponseStatus *)sdkResponseStatus {
    HveResponseStatus *ret = self.new;
    ret.requestUrl = NSE_BOX(sdkResponseStatus->request_url);
    ret.statusCode = sdkResponseStatus->status_code;
    ret.statusString = NSE_BOX(sdkResponseStatus->status_string);
    ret.subStatusCode = NSE_BOX(sdkResponseStatus->sub_status_code);
    return ret;
}

@end

#pragma mark - ActivateStatus

@implementation HveActivateStatus

+ (instancetype)from:(HVEActivateStatus *)sdkActivateStatus {
    HveActivateStatus *ret = self.new;
    ret.activated = sdkActivateStatus->activated;
    return ret;
}

@end

#pragma mark - PublicKey

@implementation HvePublicKey

+ (instancetype)from:(HVEPublicKey *)sdkPublicKey {
    HvePublicKey *ret = self.new;
    ret.key = NSE_BOX(sdkPublicKey->key);
    return ret;
}

- (HVEPublicKey *)to {
    HVEPublicKey *ret = g_new0(HVEPublicKey, 1);
    hve_public_key_set_key(ret, (gchar *)self.key.UTF8String);
    return ret;
}

@end

#pragma mark - Challenge

@implementation HveChallenge

+ (instancetype)from:(HVEChallenge *)sdkChallenge {
    HveChallenge *ret = self.new;
    ret.key = NSE_BOX(sdkChallenge->key);
    return ret;
}

@end

#pragma mark - ActivateInfo

@implementation HveActivateInfo

+ (instancetype)from:(HVEActivateInfo *)sdkActivateInfo {
    HveActivateInfo *ret = self.new;
    ret.password = NSE_BOX(sdkActivateInfo->password);
    return ret;
}

- (HVEActivateInfo *)to {
    HVEActivateInfo *ret = g_new0(HVEActivateInfo, 1);
    hve_activate_info_set_password(ret, (gchar *)self.password.UTF8String);
    return ret;
}

@end

#pragma mark - DeviceInfo

@implementation HveDeviceInfo

+ (instancetype)from:(HVEDeviceInfo *)sdkDeviceInfo {
    HveDeviceInfo *ret = self.new;
    ret.deviceName = NSE_BOX(sdkDeviceInfo->device_name);
    ret.deviceId = NSE_BOX(sdkDeviceInfo->device_id);
    ret.deviceDescription = NSE_BOX(sdkDeviceInfo->device_description);
    ret.deviceLocation = NSE_BOX(sdkDeviceInfo->device_location);
    ret.systemContact = NSE_BOX(sdkDeviceInfo->system_contact);
    ret.model = NSE_BOX(sdkDeviceInfo->model);
    ret.serialNumber = NSE_BOX(sdkDeviceInfo->serial_number);
    ret.macAddress = NSE_BOX(sdkDeviceInfo->mac_address);
    ret.firmwareVersion = NSE_BOX(sdkDeviceInfo->firmware_version);
    ret.firmwareReleasedDate = NSE_BOX(sdkDeviceInfo->firmware_released_date);
    ret.bootVersion = NSE_BOX(sdkDeviceInfo->boot_version);
    ret.bootReleasedDate = NSE_BOX(sdkDeviceInfo->boot_released_date);
    ret.hardwareVersion = NSE_BOX(sdkDeviceInfo->hardware_version);
    ret.encoderVersion = NSE_BOX(sdkDeviceInfo->encoder_version);
    ret.encoderReleasedDate = NSE_BOX(sdkDeviceInfo->encoder_released_date);
    ret.decoderVersion = NSE_BOX(sdkDeviceInfo->decoder_version);
    ret.decoderReleasedDate = NSE_BOX(sdkDeviceInfo->decoder_released_date);
    ret.deviceType = NSE_BOX(sdkDeviceInfo->device_type);
    ret.telecontrolId = sdkDeviceInfo->telecontrol_id;
    ret.supportBeep = sdkDeviceInfo->support_beep;
    ret.firmwareVersionInfo = NSE_BOX(sdkDeviceInfo->firmware_version_info);
    return ret;
}

@end

#pragma mark - Time

@implementation HveTime

+ (instancetype)from:(HVETime *)sdkTime {
    HveTime *ret = self.new;
    ret.timeMode = NSE_BOX(sdkTime->time_mode);
    ret.localTime = NSE_BOX(sdkTime->local_time);
    ret.timeZone = NSE_BOX(sdkTime->time_zone);
    return ret;
}

- (HVETime *)to {
    HVETime *ret = g_new0(HVETime, 1);
    hve_time_set_time_mode(ret, (gchar *)self.timeMode.UTF8String);
    hve_time_set_local_time(ret, (gchar *)self.localTime.UTF8String);
    hve_time_set_time_zone(ret, (gchar *)self.timeZone.UTF8String);
    return ret;
}

@end

#pragma mark - NetworkInterface

@implementation HveNetworkInterface

+ (instancetype)from:(HVENetworkInterface *)sdkNetworkInterface {
    HveNetworkInterface *ret = self.new;
    ret.id = NSE_BOX(sdkNetworkInterface->id);
    ret.defaultConnection = sdkNetworkInterface->default_connection;
    ret.macAddress = NSE_BOX(sdkNetworkInterface->mac_address);
    return ret;
}

@end

@implementation NSArray (HveNetworkInterface)

+ (instancetype)hveNetworkInterfaceFrom:(GList *)sdkNetworkInterfaces {
    NSMutableArray<HveNetworkInterface *> *ret = NSMutableArray.array;
    
    for (GList *sdkNetworkInterface = sdkNetworkInterfaces; sdkNetworkInterface != NULL; sdkNetworkInterface = sdkNetworkInterface->next) {
        HveNetworkInterface *networkInterface = [HveNetworkInterface from:sdkNetworkInterface->data];
        [ret addObject:networkInterface];
    }
    
    return ret;
}

@end

#pragma mark - WPA

@implementation HveWPA

+ (instancetype)from:(HVEWPA *)sdkWPA {
    HveWPA *ret = self.new;
    ret.algorithmType = NSE_BOX(sdkWPA->algorithm_type);
    ret.sharedKey = NSE_BOX(sdkWPA->shared_key);
    ret.wpaKeyLength = sdkWPA->wpa_key_length;
    return ret;
}

- (HVEWPA *)to {
    HVEWPA *ret = g_new0(HVEWPA, 1);
    hve_wpa_set_algorithm_type(ret, (gchar *)self.algorithmType.UTF8String);
    hve_wpa_set_shared_key(ret, (gchar *)self.sharedKey.UTF8String);
    ret->wpa_key_length = (gint)self.wpaKeyLength;
    return ret;
}

@end

#pragma mark - WirelessSecurity

@implementation HveWirelessSecurity

+ (instancetype)from:(HVEWirelessSecurity *)sdkWirelessSecurity {
    HveWirelessSecurity *ret = self.new;
    ret.securityMode = NSE_BOX(sdkWirelessSecurity->security_mode);
    ret.wpa = [HveWPA from:sdkWirelessSecurity->wpa];
    return ret;
}

- (HVEWirelessSecurity *)to {
    HVEWirelessSecurity *ret = g_new0(HVEWirelessSecurity, 1);
    hve_wireless_security_set_security_mode(ret, (gchar *)self.securityMode.UTF8String);
    ret->wpa = self.wpa.to;
    return ret;
}

@end

#pragma mark - Wireless

@implementation HveWireless

+ (instancetype)from:(HVEWireless *)sdkWireless {
    HveWireless *ret = self.new;
    ret.enabled = sdkWireless->enabled;
    ret.ssid = NSE_BOX(sdkWireless->ssid);
    ret.wirelessSecurity = [HveWirelessSecurity from:sdkWireless->wireless_security];
    return ret;
}

- (HVEWireless *)to {
    HVEWireless *ret = g_new0(HVEWireless, 1);
    ret->enabled = self.enabled;
    hve_wireless_set_ssid(ret, (gchar *)self.ssid.UTF8String);
    ret->wireless_security = self.wirelessSecurity.to;
    return ret;
}

@end

#pragma mark - IPv6Mode

@implementation HveIPv6Mode

+ (instancetype)from:(HVEIPv6Mode *)sdkIPv6Mode {
    HveIPv6Mode *ret = self.new;
    ret.ipv6AddressingType = NSE_BOX(sdkIPv6Mode->ipv6_addressing_type);
    return ret;
}

- (HVEIPv6Mode *)to {
    HVEIPv6Mode *ret = g_new0(HVEIPv6Mode, 1);
    hve_ipv6_mode_set_ipv6_addressing_type(ret, (gchar *)self.ipv6AddressingType.UTF8String);
    return ret;
}

@end

#pragma mark - IPAddress

@implementation HveIPAddress

+ (instancetype)from:(HVEIPAddress *)sdkIPAddress {
    HveIPAddress *ret = self.new;
    ret.ipVersion = NSE_BOX(sdkIPAddress->ip_version);
    ret.addressingType = NSE_BOX(sdkIPAddress->addressing_type);
    ret.ipv6Mode = [HveIPv6Mode from:sdkIPAddress->ipv6_mode];
    return ret;
}

- (HVEIPAddress *)to {
    HVEIPAddress *ret = g_new0(HVEIPAddress, 1);
    hve_ip_address_set_ip_version(ret, (gchar *)self.ipVersion.UTF8String);
    hve_ip_address_set_addressing_type(ret, (gchar *)self.addressingType.UTF8String);
    ret->ipv6_mode = self.ipv6Mode.to;
    return ret;
}

@end

#pragma mark - Video

@implementation HveVideo

+ (instancetype)from:(HVEVideo *)sdkVideo {
    HveVideo *ret = self.new;
    ret.videoCodecType = NSE_BOX(sdkVideo->video_codec_type);
    ret.videoResolutionWidth = sdkVideo->video_resolution_width;
    ret.videoResolutionHeight = sdkVideo->video_resolution_height;
    ret.videoQualityControlType = NSE_BOX(sdkVideo->video_quality_control_type);
    ret.maxFrameRate = sdkVideo->max_frame_rate;
    return ret;
}

- (HVEVideo *)to {
    HVEVideo *ret = g_new0(HVEVideo, 1);
    hve_video_set_video_codec_type(ret, (gchar *)self.videoCodecType.UTF8String);
    ret->video_resolution_width = (gint)self.videoResolutionWidth;
    ret->video_resolution_height = (gint)self.videoResolutionHeight;
    hve_video_set_video_quality_control_type(ret, (gchar *)self.videoQualityControlType.UTF8String);
    ret->max_frame_rate = (gint)self.maxFrameRate;
    return ret;
}

@end

#pragma mark - Audio

@implementation HveAudio

+ (instancetype)from:(HVEAudio *)sdkAudio {
    HveAudio *ret = self.new;
    ret.enabled = sdkAudio->enabled;
    ret.audioCompressionType = NSE_BOX(sdkAudio->audio_compression_type);
    return ret;
}

- (HVEAudio *)to {
    HVEAudio *ret = g_new0(HVEAudio, 1);
    ret->enabled = self.enabled;
    hve_audio_set_audio_compression_type(ret, (gchar *)self.audioCompressionType.UTF8String);
    return ret;
}

@end

#pragma mark - StreamingChannel

@implementation HveStreamingChannel

+ (instancetype)from:(HVEStreamingChannel *)sdkStreamingChannel {
    HveStreamingChannel *ret = self.new;
    ret.video = [HveVideo from:sdkStreamingChannel->video];
    ret.audio = [HveAudio from:sdkStreamingChannel->audio];
    return ret;
}

- (HVEStreamingChannel *)to {
    HVEStreamingChannel *ret = g_new0(HVEStreamingChannel, 1);
    ret->video = self.video.to;
    ret->audio = self.audio.to;
    return ret;
}

@end

#pragma mark - Grid

@implementation HveGrid

+ (instancetype)from:(HVEGrid *)sdkGrid {
    HveGrid *ret = self.new;
    ret.rowGranularity = sdkGrid->row_granularity;
    ret.columnGranularity = sdkGrid->column_granularity;
    return ret;
}

- (HVEGrid *)to {
    HVEGrid *ret = g_new0(HVEGrid, 1);
    ret->row_granularity = (gint)self.rowGranularity;
    ret->column_granularity = (gint)self.columnGranularity;
    return ret;
}

@end

#pragma mark - Layout

@implementation HveLayout

+ (instancetype)from:(HVELayout *)sdkLayout {
    HveLayout *ret = self.new;
    ret.gridMap = NSE_BOX(sdkLayout->grid_map);
    return ret;
}

- (HVELayout *)to {
    HVELayout *ret = g_new0(HVELayout, 1);
    hve_layout_set_grid_map(ret, (gchar *)self.gridMap.UTF8String);
    return ret;
}

@end

#pragma mark - MotionDetectionLayout

@implementation HveMotionDetectionLayout

+ (instancetype)from:(HVEMotionDetectionLayout *)sdkMotionDetectionLayout {
    HveMotionDetectionLayout *ret = self.new;
    ret.sensitivityLevel = sdkMotionDetectionLayout->sensitivity_level;
    ret.layout = [HveLayout from:sdkMotionDetectionLayout->layout];
    return ret;
}

- (HVEMotionDetectionLayout *)to {
    HVEMotionDetectionLayout *ret = g_new0(HVEMotionDetectionLayout, 1);
    ret->sensitivity_level = (gint)self.sensitivityLevel;
    ret->layout = self.layout.to;
    return ret;
}

@end

#pragma mark - MotionDetection

@implementation HveMotionDetection

+ (instancetype)from:(HVEMotionDetection *)sdkMotionDetection {
    HveMotionDetection *ret = self.new;
    ret.enabled = sdkMotionDetection->enabled;
    ret.grid = [HveGrid from:sdkMotionDetection->grid];
    ret.motionDetectionLayout = [HveMotionDetectionLayout from:sdkMotionDetection->motion_detection_layout];
    return ret;
}

- (HVEMotionDetection *)to {
    HVEMotionDetection *ret = g_new0(HVEMotionDetection, 1);
    ret->enabled = self.enabled;
    ret->grid = self.grid.to;
    ret->motion_detection_layout = self.motionDetectionLayout.to;
    return ret;
}

@end

#pragma mark - Endpoint

@implementation HveEndpoint

+ (instancetype)from:(HVEEndpoint *)sdkEndpoint {
    HveEndpoint *ret = self.new;
    ret.name = NSE_BOX(sdkEndpoint->name);
    ret.host = NSE_BOX(sdkEndpoint->host);
    ret.ip = NSE_BOX(sdkEndpoint->ip);
    ret.port = sdkEndpoint->port;
    ret.path = NSE_BOX(sdkEndpoint->path);
    return ret;
}

- (HVEEndpoint *)to {
    HVEEndpoint *ret = g_new0(HVEEndpoint, 1);
    hve_endpoint_set_name(ret, (gchar *)self.name.UTF8String);
    hve_endpoint_set_host(ret, (gchar *)self.host.UTF8String);
    hve_endpoint_set_ip(ret, (gchar *)self.ip.UTF8String);
    ret->port = (gint)self.port;
    hve_endpoint_set_path(ret, (gchar *)self.path.UTF8String);
    return ret;
}

@end
