//
//  HveInit.m
//  HikVisionExt
//
//  Created by Dan on 26.10.2021.
//

#import "HveInit.h"

@implementation HveInit

+ (void)initialize {
    (void)NseInit.class;
    (void)SoeInit.class;
    (void)TlsInit.class;
    (void)GneInit.class;
    (void)XmleInit.class;
    (void)AvhInit.class;
    
    hve_init();
}

@end
