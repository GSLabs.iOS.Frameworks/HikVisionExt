//
//  HveClient.m
//  HikVisionExt
//
//  Created by Dan on 17.09.2021.
//

#import "HveClient.h"

@implementation HveClient

- (void)dealloc {
    g_object_unref(self.object);
}

+ (instancetype)clientWithBase:(NSString *)base username:(NSString *)username password:(NSString *)password {
    HveClient *ret = self.new;
    g_autoptr(SoupURI) sdkBase = soup_uri_new(base.UTF8String);
    ret.object = hve_soe_session_new(sdkBase, (gchar *)username.UTF8String, (gchar *)password.UTF8String);
    ret.base = base;
    ret.username = username;
    ret.password = password;
    return ret;
}

+ (instancetype)clientWithEndpoint:(HveEndpoint *)endpoint username:(NSString *)username password:(NSString *)password {
    NSURLComponents *base = NSURLComponents.new;
    base.scheme = @"http";
    base.host = endpoint.ip;
    base.port = @(endpoint.port);
    base.path = endpoint.path;
    HveClient *ret = [self clientWithBase:base.string username:username password:password];
    return ret;
}

- (void)abort {
    soup_session_abort(SOUP_SESSION(self.object));
}

#pragma mark - ActivateStatusGet

- (HveActivateStatus *)activateStatusGetSync:(NSError **)error {
    HveActivateStatus *ret = nil;
    g_autoptr(HVEActivateStatus) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_activate_status_get_sync(self.object, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveActivateStatus from:sdkRet];
    }
    
    return ret;
}

- (void)activateStatusGetAsync:(HveClientActivateStatusGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveActivateStatus *activateStatus = [self activateStatusGetSync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, activateStatus, error);
        }];
    });
}

#pragma mark - ChallengeGet

- (HveChallenge *)challengeGetSync:(HvePublicKey *)publicKey error:(NSError **)error {
    HveChallenge *ret = nil;
    g_autoptr(HVEChallenge) sdkRet = NULL;
    g_autoptr(HVEPublicKey) sdkPublicKey = publicKey.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_challenge_get_sync(self.object, sdkPublicKey, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveChallenge from:sdkRet];
    }
    
    return ret;
}

- (void)challengeGetAsync:(HvePublicKey *)publicKey completion:(HveClientChallengeGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveChallenge *challenge = [self challengeGetSync:publicKey error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, challenge, error);
        }];
    });
}

#pragma mark - Activate

- (HveResponseStatus *)activateSync:(HveActivateInfo *)activateInfo error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEActivateInfo) sdkActivateInfo = activateInfo.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_activate_sync(self.object, sdkActivateInfo, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)activateAsync:(HveActivateInfo *)activateInfo completion:(HveClientActivateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self activateSync:activateInfo error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - Reboot

- (HveResponseStatus *)rebootSync:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_reboot_sync(self.object, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)rebootAsync:(HveClientRebootAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self rebootSync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - DeviceInfoGet

- (HveDeviceInfo *)deviceInfoGetSync:(NSError **)error {
    HveDeviceInfo *ret = nil;
    g_autoptr(HVEDeviceInfo) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_device_info_get_sync(self.object, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveDeviceInfo from:sdkRet];
    }
    
    return ret;
}

- (void)deviceInfoGetAsync:(HveClientDeviceInfoGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveDeviceInfo *deviceInfo = [self deviceInfoGetSync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, deviceInfo, error);
        }];
    });
}

#pragma mark - TimeUpdate

- (HveResponseStatus *)timeUpdateSync:(HveTime *)time error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVETime) sdkTime = time.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_time_update_sync(self.object, sdkTime, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)timeUpdateAsync:(HveTime *)time completion:(HveClientTimeUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self timeUpdateSync:time error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - NetworkInterfacesGet

- (NSArray<HveNetworkInterface *> *)networkInterfacesGetSync:(NSError **)error {
    NSArray<HveNetworkInterface *> *ret = nil;
    g_autolist(HVENetworkInterface) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (hve_soe_session_network_interfaces_get_sync(self.object, &sdkRet, &sdkError)) {
        ret = [NSArray hveNetworkInterfaceFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)networkInterfacesGetAsync:(HveClientNetworkInterfacesGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSArray<HveNetworkInterface *> *networkInterfaces = [self networkInterfacesGetSync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, networkInterfaces, error);
        }];
    });
}

#pragma mark - WirelessGet

- (HveWireless *)wirelessGetSync:(NSString *)id error:(NSError **)error {
    HveWireless *ret = nil;
    g_autoptr(HVEWireless) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_wireless_get_sync(self.object, (gchar *)id.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveWireless from:sdkRet];
    }
    
    return ret;
}

- (void)wirelessGetAsync:(NSString *)id completion:(HveClientWirelessGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveWireless *wireless = [self wirelessGetSync:id error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, wireless, error);
        }];
    });
}

#pragma mark - WirelessUpdate

- (HveResponseStatus *)wirelessUpdateSync:(NSString *)id wireless:(HveWireless *)wireless error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEWireless) sdkWireless = wireless.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_wireless_update_sync(self.object, (gchar *)id.UTF8String, sdkWireless, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)wirelessUpdateAsync:(NSString *)id wireless:(HveWireless *)wireless completion:(HveClientWirelessUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self wirelessUpdateSync:id wireless:wireless error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - WirelessUpdateV1

- (BOOL)wirelessUpdateV1Sync:(NSString *)id wireless:(HveWireless *)wireless error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(HVEWireless) sdkWireless = wireless.to;
    g_autoptr(GError) sdkError = NULL;
    
    if (hve_soe_session_wireless_update_v1_sync(self.object, (gchar *)id.UTF8String, sdkWireless, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)wirelessUpdateV1Async:(NSString *)id wireless:(HveWireless *)wireless completion:(HveClientWirelessUpdateV1AsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self wirelessUpdateV1Sync:id wireless:wireless error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - IPAddressGet

- (HveIPAddress *)ipAddressGetSync:(NSString *)id error:(NSError **)error {
    HveIPAddress *ret = nil;
    g_autoptr(HVEIPAddress) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_ip_address_get_sync(self.object, (gchar *)id.UTF8String, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveIPAddress from:sdkRet];
    }
    
    return ret;
}

- (void)ipAddressGetAsync:(NSString *)id completion:(HveClientIPAddressGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveIPAddress *ipAddress = [self ipAddressGetSync:id error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, ipAddress, error);
        }];
    });
}

#pragma mark - IPAddressUpdate

- (HveResponseStatus *)ipAddressUpdateSync:(NSString *)id ipAddress:(HveIPAddress *)ipAddress error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEIPAddress) sdkIpAddress = ipAddress.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_ip_address_update_sync(self.object, (gchar *)id.UTF8String, sdkIpAddress, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)ipAddressUpdateAsync:(NSString *)id ipAddress:(HveIPAddress *)ipAddress completion:(HveClientIPAddressUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self ipAddressUpdateSync:id ipAddress:ipAddress error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - StreamingChannelUpdate

- (HveResponseStatus *)streamingChannelUpdateSync:(NSString *)id streamingChannel:(HveStreamingChannel *)streamingChannel error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEStreamingChannel) sdkStreamingChannel = streamingChannel.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_streaming_channel_update_sync(self.object, (gchar *)id.UTF8String, sdkStreamingChannel, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)streamingChannelUpdateAsync:(NSString *)id streamingChannel:(HveStreamingChannel *)streamingChannel completion:(HveClientStreamingChannelUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self streamingChannelUpdateSync:id streamingChannel:streamingChannel error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - MotionDetectionUpdate

- (HveResponseStatus *)motionDetectionUpdateSync:(NSString *)id motionDetection:(HveMotionDetection *)motionDetection error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEMotionDetection) sdkMotionDetection = motionDetection.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_motion_detection_update_sync(self.object, (gchar *)id.UTF8String, sdkMotionDetection, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)motionDetectionUpdateAsync:(NSString *)id motionDetection:(HveMotionDetection *)motionDetection completion:(HveClientMotionDetectionUpdateAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self motionDetectionUpdateSync:id motionDetection:motionDetection error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - ActivateV1

- (HveResponseStatus *)activateV1Sync:(HveActivateInfo *)activateInfo error:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(HVEActivateInfo) sdkActivateInfo = activateInfo.to;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_activate_v1_sync(self.object, sdkActivateInfo, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)activateV1Async:(HveActivateInfo *)activateInfo completion:(HveClientActivateV1AsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self activateV1Sync:activateInfo error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

#pragma mark - TimeUpdateV1

- (HveResponseStatus *)timeUpdateV1Sync:(NSError **)error {
    HveResponseStatus *ret = nil;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if ((sdkRet = hve_soe_session_time_update_v1_sync(self.object, &sdkError)) == NULL) {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    } else {
        ret = [HveResponseStatus from:sdkRet];
    }
    
    return ret;
}

- (void)timeUpdateV1Async:(HveClientTimeUpdateV1AsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        HveResponseStatus *responseStatus = [self timeUpdateV1Sync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, responseStatus, error);
        }];
    });
}

@end
