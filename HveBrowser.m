//
//  HveBrowser.m
//  HikVisionExt
//
//  Created by Dan on 18.09.2021.
//

#import "HveBrowser.h"

@implementation HveBrowserDelegates

@end

@implementation HveBrowser

+ (instancetype)browserWithType:(NSString *)type {
    HveBrowser *ret = self.new;
    ret.type = type;
    ret.delegates = [HveBrowserDelegates.alloc initWithStorage:NSPointerArray.weakObjectsPointerArray];
    ret.delegate = ret;
    ret.services = NSMutableSet.set;
    return ret;
}

- (void)start {
    [self stop];
    [self searchForServicesOfType:self.type inDomain:@"local"];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser didFindService:(NSNetService *)service moreComing:(BOOL)moreComing {
    [self.services addObject:service];
    service.delegate = self;
    [service resolveWithTimeout:10.0];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser didRemoveService:(NSNetService *)service moreComing:(BOOL)moreComing {
    [self.delegates hveBrowser:self endpointRemoved:service.name];
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender {
    [self.services removeObject:sender];
    
    for (NSData *address in sender.addresses) {
        struct sockaddr *addr = (struct sockaddr *)address.bytes;
        gchar ip[INET_ADDRSTRLEN];
        gchar ip6[INET6_ADDRSTRLEN];
        
        NSDictionary<NSString *, NSData *> *txt = [NSNetService dictionaryFromTXTRecordData:sender.TXTRecordData];
        NSData *path = txt[@"path"];
        
        HveEndpoint *endpoint = HveEndpoint.new;
        endpoint.name = sender.name;
        endpoint.host = sender.hostName;
        endpoint.ip = (addr->sa_family == AF_INET) ? @(inet_ntop(AF_INET, &((struct sockaddr_in *)addr)->sin_addr, ip, INET_ADDRSTRLEN)) : @(inet_ntop(AF_INET6, &((struct sockaddr_in6 *)addr)->sin6_addr, ip6, INET6_ADDRSTRLEN));
        endpoint.port = sender.port;
        endpoint.path = [NSString.alloc initWithData:path encoding:NSUTF8StringEncoding];
        
        [self.delegates hveBrowser:self endpointFound:endpoint];
    }
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary<NSString *, NSNumber *> *)errorDict {
    [self.services removeObject:sender];
}

@end
