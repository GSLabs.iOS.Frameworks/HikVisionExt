# HikVisionExt

IP camera configuration SDK for iOS.

- [Installation](#installation)
- [API](#api)
    - [Client](#client)
    - [Browser](#browser)
- [Examples](#examples)
    - [Activate](#activate)
    - [Connect to Wi-Fi](#connect-to-wi-fi)
    - [Enable DHCP on LAN interface](#enable-dhcp-on-lan-interface)
    - [Get camera info](#get-camera-info)
    - [Video/Audio codec](#videoaudio-codec)
    - [Date and time](#date-and-time)

## Installation

Add these lines to your `Podfile` and run `pod install` command.

```ruby
install! "cocoapods",
  :preserve_pod_file_structure => true

pod "GLibExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GLibExt.git", :tag => "1.1", :submodules => true
pod "SoupExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/SoupExt.git", :tag => "1.1", :submodules => true
pod "GnuTLSExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GnuTLSExt.git", :tag => "1.1", :submodules => true
pod "GLibNetworkingExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GLibNetworkingExt.git", :tag => "1.1", :submodules => true
pod "XMLExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/XMLExt.git", :tag => "1.1", :submodules => true
pod "AvahiExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/AvahiExt.git", :tag => "1.1", :submodules => true
pod "HikVisionExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/HikVisionExt.git", :tag => "1.1", :submodules => true
```

## API

The API consists of 2 main interfaces - Client and Browser.

### Client

`HveClient` class provides high level API for making HTTP requests to the camera - Activate, WirelessUpdate, IPAddressUpdate, etc. Each method has its synchronous and asynchronous version. Synchronous version ends with `Sync` suffix and blocks the current thread until request is completed. Asynchronous version ends with `Async` suffix and invokes a completion block when request is completed. All completion blocks are dispatched to the application's main thread.

```objectivec
HveClient *client = [HveClient clientWithBase:@"http://192.168.8.1" username:@"admin" password:@"qwerty-123"];
```

### Browser

`HveBrowser` class discovers the cameras in local network and resolves their IP addresses. Resolved addresses can be used by `HveClient` instances for making HTTP requests to the cameras.

```objectivec
self.browser = [HveBrowser browserWithType:@"_psia._tcp"];
[self.browser.delegates addObject:self];
[self.browser start];

- (void)hveBrowser:(HveBrowser *)sender endpointFound:(HveEndpoint *)endpoint {
    HveClient *client = [HveClient clientWithEndpoint:endpoint username:@"admin" password:@"qwerty-123"];
}
```

## Examples

First you need to reset the camera by holding the side button for 5 seconds. Then the camera will create an access point with parameters, specified on its body. You need to connect it using Settings application or `NEHotspotConfigurationManager` API.

### Activate

Most of request are protected with Digest auth. We need to specify the credentials for these challenges. Username is always `admin` so we only need to set the password up to 16 characters length. For security reasons the password should be encrypted. `V1` method does this encryption for you.

```objectivec
HveActivateInfo *activateInfo = HveActivateInfo.new;
activateInfo.password = @"qwerty-123";

[client activateV1Async:activateInfo completion:^(HveResponseStatus *responseStatus, NSError *error) {
    if (responseStatus == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success
    }
}];
```

### Connect to Wi-Fi

Join the camera to the network named `My network` with password of `12345678`. Camera shuts down its access point on success.

```objectivec
HveWPA *wpa = HveWPA.new;
wpa.algorithmType = @"TKIP";
wpa.sharedKey = @"12345678";
wpa.wpaKeyLength = wpa.sharedKey.length;

HveWirelessSecurity *wirelessSecurity = HveWirelessSecurity.new;
wirelessSecurity.securityMode = @"WPA2-personal";
wirelessSecurity.wpa = wpa;

HveWireless *wireless = HveWireless.new;
wireless.enabled = YES;
wireless.ssid = @"My network";
wireless.wirelessSecurity = wirelessSecurity;

[client wirelessUpdateAsync:@"2" wireless:wireless completion:^(HveResponseStatus *responseStatus, NSError *error) {
    if (responseStatus == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success
    }
}];
```

### Enable DHCP on LAN interface

Allow the camera to obtain dynamic address in local network instead of fixed one - `192.168.1.64`. This is required when multiple cameras are in the same network. To apply the changes reboot is required.

```objectivec
HveIPv6Mode *ipv6Mode = HveIPv6Mode.new;
ipv6Mode.ipv6AddressingType = @"dhcp";

HveIPAddress *ipAddress = HveIPAddress.new;
ipAddress.ipVersion = @"dual";
ipAddress.addressingType = @"dynamic";
ipAddress.ipv6Mode = ipv6Mode;

[client ipAddressUpdateAsync:@"1" ipAddress:ipAddress completion:^(HveResponseStatus *responseStatus, NSError *error) {
    if (responseStatus == nil) {
        NSLog(error.localizedDescription);
    } else {
        [client rebootAsync:nil];
    }
}];
```

### Get camera info

Get the information about camera such as name, model and serial number.

```objectivec
[client deviceInfoGetAsync:^(HveDeviceInfo *deviceInfo, NSError *error) {
    if (deviceInfo == nil) {
        NSLog(error.localizedDescription);
    } else {
        NSLog(deviceInfo.model);
    }
}];
```

### Video/Audio codec

Change the video stream encoding. Currently supported values are `H.264` and `H.265`.

```objectivec
HveVideo *video = HveVideo.new;
video.videoCodecType = @"H.264";
video.videoResolutionWidth = 1920;
video.videoResolutionHeight = 1080;
video.videoQualityControlType = @"VBR";
video.maxFrameRate = 2500;

HveAudio *audio = HveAudio.new;
audio.enabled = YES;
audio.audioCompressionType = @"G.726";

HveStreamingChannel *streamingChannel = HveStreamingChannel.new;
streamingChannel.video = video;
streamingChannel.audio = audio;

[client streamingChannelUpdateAsync:@"101" streamingChannel:streamingChannel completion:^(HveResponseStatus *responseStatus, NSError *error) {
    if (responseStatus == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success
    }
}];
```

### Date and time

Change the camera screen time. `V1` method sets it to the client local time.

```objectivec
[client timeUpdateV1Async:^(HveResponseStatus *responseStatus, NSError *error) {
    if (responseStatus == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success
    }
}];
```
