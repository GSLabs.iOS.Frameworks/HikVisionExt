//
//  HveClient.h
//  HikVisionExt
//
//  Created by Dan on 17.09.2021.
//

#import "HveMain.h"
#import "HveSchema.h"

@interface HveClient : NseObject

@property HVESOESession *object;
@property NSString *base;
@property NSString *username;
@property NSString *password;

+ (instancetype)clientWithBase:(NSString *)base username:(NSString *)username password:(NSString *)password;
+ (instancetype)clientWithEndpoint:(HveEndpoint *)endpoint username:(NSString *)username password:(NSString *)password;

- (void)abort;

#pragma mark - ActivateStatusGet

- (HveActivateStatus *)activateStatusGetSync:(NSError **)error;

typedef void (^HveClientActivateStatusGetAsyncCompletion)(HveActivateStatus *activateStatus, NSError *error);
- (void)activateStatusGetAsync:(HveClientActivateStatusGetAsyncCompletion)completion;

#pragma mark - ChallengeGet

- (HveChallenge *)challengeGetSync:(HvePublicKey *)publicKey error:(NSError **)error;

typedef void (^HveClientChallengeGetAsyncCompletion)(HveChallenge *challenge, NSError *error);
- (void)challengeGetAsync:(HvePublicKey *)publicKey completion:(HveClientChallengeGetAsyncCompletion)completion;

#pragma mark - Activate

- (HveResponseStatus *)activateSync:(HveActivateInfo *)activateInfo error:(NSError **)error;

typedef void (^HveClientActivateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)activateAsync:(HveActivateInfo *)activateInfo completion:(HveClientActivateAsyncCompletion)completion;

#pragma mark - Reboot

- (HveResponseStatus *)rebootSync:(NSError **)error;

typedef void (^HveClientRebootAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)rebootAsync:(HveClientRebootAsyncCompletion)completion;

#pragma mark - DeviceInfoGet

- (HveDeviceInfo *)deviceInfoGetSync:(NSError **)error;

typedef void (^HveClientDeviceInfoGetAsyncCompletion)(HveDeviceInfo *deviceInfo, NSError *error);
- (void)deviceInfoGetAsync:(HveClientDeviceInfoGetAsyncCompletion)completion;

#pragma mark - TimeUpdate

- (HveResponseStatus *)timeUpdateSync:(HveTime *)time error:(NSError **)error;

typedef void (^HveClientTimeUpdateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)timeUpdateAsync:(HveTime *)time completion:(HveClientTimeUpdateAsyncCompletion)completion;

#pragma mark - NetworkInterfacesGet

- (NSArray<HveNetworkInterface *> *)networkInterfacesGetSync:(NSError **)error;

typedef void (^HveClientNetworkInterfacesGetAsyncCompletion)(NSArray<HveNetworkInterface *> *networkInterfaces, NSError *error);
- (void)networkInterfacesGetAsync:(HveClientNetworkInterfacesGetAsyncCompletion)completion;

#pragma mark - WirelessGet

- (HveWireless *)wirelessGetSync:(NSString *)id error:(NSError **)error;

typedef void (^HveClientWirelessGetAsyncCompletion)(HveWireless *wireless, NSError *error);
- (void)wirelessGetAsync:(NSString *)id completion:(HveClientWirelessGetAsyncCompletion)completion;

#pragma mark - WirelessUpdate

- (HveResponseStatus *)wirelessUpdateSync:(NSString *)id wireless:(HveWireless *)wireless error:(NSError **)error;

typedef void (^HveClientWirelessUpdateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)wirelessUpdateAsync:(NSString *)id wireless:(HveWireless *)wireless completion:(HveClientWirelessUpdateAsyncCompletion)completion;

#pragma mark - WirelessUpdateV1

- (BOOL)wirelessUpdateV1Sync:(NSString *)id wireless:(HveWireless *)wireless error:(NSError **)error;

typedef void (^HveClientWirelessUpdateV1AsyncCompletion)(NSError *error);
- (void)wirelessUpdateV1Async:(NSString *)id wireless:(HveWireless *)wireless completion:(HveClientWirelessUpdateV1AsyncCompletion)completion;

#pragma mark - IPAddressGet

- (HveIPAddress *)ipAddressGetSync:(NSString *)id error:(NSError **)error;

typedef void (^HveClientIPAddressGetAsyncCompletion)(HveIPAddress *ipAddress, NSError *error);
- (void)ipAddressGetAsync:(NSString *)id completion:(HveClientIPAddressGetAsyncCompletion)completion;

#pragma mark - IPAddressUpdate

- (HveResponseStatus *)ipAddressUpdateSync:(NSString *)id ipAddress:(HveIPAddress *)ipAddress error:(NSError **)error;

typedef void (^HveClientIPAddressUpdateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)ipAddressUpdateAsync:(NSString *)id ipAddress:(HveIPAddress *)ipAddress completion:(HveClientIPAddressUpdateAsyncCompletion)completion;

#pragma mark - StreamingChannelUpdate

- (HveResponseStatus *)streamingChannelUpdateSync:(NSString *)id streamingChannel:(HveStreamingChannel *)streamingChannel error:(NSError **)error;

typedef void (^HveClientStreamingChannelUpdateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)streamingChannelUpdateAsync:(NSString *)id streamingChannel:(HveStreamingChannel *)streamingChannel completion:(HveClientStreamingChannelUpdateAsyncCompletion)completion;

#pragma mark - MotionDetectionUpdate

- (HveResponseStatus *)motionDetectionUpdateSync:(NSString *)id motionDetection:(HveMotionDetection *)motionDetection error:(NSError **)error;

typedef void (^HveClientMotionDetectionUpdateAsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)motionDetectionUpdateAsync:(NSString *)id motionDetection:(HveMotionDetection *)motionDetection completion:(HveClientMotionDetectionUpdateAsyncCompletion)completion;

#pragma mark - ActivateV1

- (HveResponseStatus *)activateV1Sync:(HveActivateInfo *)activateInfo error:(NSError **)error;

typedef void (^HveClientActivateV1AsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)activateV1Async:(HveActivateInfo *)activateInfo completion:(HveClientActivateV1AsyncCompletion)completion;

#pragma mark - TimeUpdateV1

- (HveResponseStatus *)timeUpdateV1Sync:(NSError **)error;

typedef void (^HveClientTimeUpdateV1AsyncCompletion)(HveResponseStatus *responseStatus, NSError *error);
- (void)timeUpdateV1Async:(HveClientTimeUpdateV1AsyncCompletion)completion;

@end
